using System.Net;
using System.Linq;


namespace Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// Convert an IPAdress objecto to a uint
        /// </summary>
        public static uint ToUInt(this IPAddress ip)
        {
            uint[] octets = (from oct in ip.GetAddressBytes() select (uint)oct).ToArray();

            // mask the array into a single integer
            uint result = 0;
            for (int i = 0; i < 4; i++) {
                int shift = 8 * (3 - i);
                result |= octets[i] << shift;
            }

            return result;
        }


        /// <summary>
        /// Convert a uint to an IPAddress object
        /// </summary>
        public static IPAddress ToIPAddress(this uint u)
        {
            // mask the number into 4-byte array for IPAddress class to parse internally
            byte[] octets = new byte[4];

            for (int i = 0; i < 4; i++) {
                int shift = 8 * (3 - i);
                octets[i] = (byte)((u >> shift) & 0xFF);
            }

            return new IPAddress(octets);
        }

        public static byte[] ToByteArray(this string s)
        {
            return (from c in s select (byte)c).ToArray();
        }
    }
}