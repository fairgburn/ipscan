﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Extensions;

namespace IPScan
{
    class Program
    {
        static void Main(string[] args)
        {
            
            // check program parameters
            if (args.Length != 0 && args.Length != 2)
            {
                Console.WriteLine("unrecognized command");
                return;
            }

            string low_end;
            string high_end;

            // grab the range to scan
            if (args.Length == 0) 
            {
                Console.Write("Low end of IP range:  ");
                low_end = Console.ReadLine();

                Console.Write("High end of IP range: ");
                high_end = Console.ReadLine();
            }
            else
            {
                low_end = args[0];
                high_end = args[1];
            }

            // run scan and measure execution time
            var start = DateTime.Now;

            var scanner = new Scanner(low_end, high_end);
            var results = scanner.Scan();

            var end = DateTime.Now;

            // use TextTable class to format output
            var table = new TextTable("IP Address", "Response time", "MAC Address");
            foreach (var result in results)
            {
                table.Add(result.Reply.Address.ToString(), result.ResponseTime, result.MAC);
            }
            table.Show();

            // force garbage collection; memory usage can be significant if scanning a large range
            GC.Collect();

            Console.WriteLine($"\nscanned {scanner.Range.Length} IPs in {(end - start).TotalSeconds} seconds");
            
            Console.ReadLine();
        }
    }
}

