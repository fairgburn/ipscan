﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;


namespace IPScan
{
    /// <summary>
    /// Provides a simple method of organizing a table into rows/columns for command line display
    /// </summary>
    public class TextTable
    {
        Column[] Columns { get; }

        private List<string[]> Rows = new List<string[]>();

        public TextTable(params string[] columns)
        {
            Columns = new Column[columns.Length];
            for (int i = 0; i < columns.Length; i++)
            {
                Columns[i] = new Column(columns[i]);
            }
        }

        public void Add(params string[] data)
        {
            if (data.Length != Columns.Length)
                throw new ArgumentException($"TextTable.Add() requires {Columns.Length} arguments");

            // update the column width
            for (int i = 0; i < Columns.Length; i++)
            {
                if (data[i].Length > Columns[i].Width)
                    Columns[i].Width = data[i].Length;
            }

            Rows.Add(data);
        }

        /// <summary>
        /// Print table contents to console
        /// </summary>
        /// <param name="padding">number of spaces after the max width of column</param>
        public void Show(int padding = 4)
        {
            Console.WriteLine("\n");
            // column names
            foreach (var col in Columns)
            {
                var sb = new StringBuilder(col.Title);
                while (sb.Length < col.Width + padding) sb.Append(' ');
                Console.Write(sb.ToString());
            }
            Console.WriteLine("\n");

            // table contents
            foreach (var row in Rows)
            {
                for (int i = 0; i < Columns.Length; i++)
                {
                    var sb = new StringBuilder(row[i]);
                    while (sb.Length < Columns[i].Width + padding) sb.Append(' ');
                    Console.Write(sb.ToString());
                }
                Console.WriteLine();
            }
        }

        //////

        class Column
        {
            public string Title { get; set; }
            public int Width { get; set; }

            public Column(string title)
            {
                Title = title;
                Width = title.Length;
            }
        }
    }

    
}
