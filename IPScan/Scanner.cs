﻿using System;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.NetworkInformation;
using System.Linq;
using System.Diagnostics;
using System.Collections.Generic;
using System.Threading.Tasks;
using Extensions;

namespace IPScan
{
    /// <summary>
    /// Provides a way to scan a network over a range of IP addresses
    /// </summary>
    public class Scanner
    {

        // properties
        public IPAddress LowAddress { get; private set; }
        public IPAddress HighAddress { get; private set; }
        public int Timeout { get; set; }
        public IPAddress[] Range { get; private set; }
        public byte[] Buffer { get; set; }
        //------------------------------

        // constructors
        private void Init(IPAddress low, IPAddress high, int timeout)
        {
            if (!ValidateRange(low, high))
            {
                throw new ArgumentException("Invalid IP range");
            }

            LowAddress = low;
            HighAddress = high;
            Timeout = timeout;

            int count = (int)(HighAddress.ToUInt() - LowAddress.ToUInt());
            Range = (from num in Enumerable.Range((int)LowAddress.ToUInt(), count)
                     select ((uint)num).ToIPAddress()).ToArray();
        }

        public Scanner(IPAddress low, IPAddress high, int timeout = 500) { Init(low, high, timeout); }
        public Scanner(string low, string high, int timeout = 500) { Init(IPAddress.Parse(low), IPAddress.Parse(high), timeout); }
        public Scanner(uint low, uint high, int timeout = 500) { Init(low.ToIPAddress(), high.ToIPAddress(), timeout); }
        //------------------------------


        /// <summary>
        /// Perform IP range scan asynchronously (in parallel) and return results to caller
        /// </summary>
        /// <returns></returns>
        public ScanResult[] Scan()
        {
            // the IP addresses which we'll scan
            // = Range

            // scan
            var replies = new Task<PingReply>[Range.Length];
            for (int i = 0; i < replies.Length; i++)
            {
                replies[i] = ScanIPAsync(Range[i]);
            }

            // return results for successful pings
            var success = from reply in replies
                          where reply.Result.Status == IPStatus.Success
                          select new ScanResult(reply.Result, MacAddress(reply.Result.Address));

            return success.ToArray();
        }
        //------------------------------


        // private helper methods
        private bool ValidateRange(IPAddress low, IPAddress high)
        {
            // convert IP addresses to integers and compare
            if (low.ToUInt() >= high.ToUInt()) return false;
            else return true;
        }

        private async Task<PingReply> ScanIPAsync(IPAddress ip)
        {
            byte[] buf = Buffer ?? new byte[32]; // array initializes to all zeros if custom Buffer is not set, this is fine

            // scan by pinging
            var ping = new Ping();
            var options = new PingOptions { DontFragment = true };

            return await ping.SendPingAsync(ip, Timeout, buf, options);
        }
  
        /// <summary>
        /// Run `arp -a {IP address}` in the background and extract the MAC address, if one is found
        /// </summary>
        private static string MacAddress(IPAddress ip)
        {
            var sinfo = new ProcessStartInfo()
            {
                FileName = "arp",
                Arguments = $"-a {ip.ToString()}",
                CreateNoWindow = true,
                RedirectStandardOutput = true
            };

            var proc = new Process() { StartInfo = sinfo };
            proc.Start();

            string arpInfo = proc.StandardOutput.ReadToEnd();

            // extract MAC address from arp results with a regex
            var rexp = new Regex("([0-9a-fA-F]{2}[:-]){5}[0-9a-fA-F]{2}");
            var match = rexp.Match(arpInfo);

            return match.Success ? match.Value : "-"; // represent no MAC address found with a dash
            
        }

        private static string MacAddress(string ip)
        {
            return MacAddress(IPAddress.Parse(ip));
        }
        //------------------------------
    }


    /// <summary>
    /// Contains the relevant information from an IP scan
    /// </summary>
    public class ScanResult
    {
        public PingReply Reply { get; set; }
        public string MAC { get; set; }
        public string ResponseTime => $"{Reply.RoundtripTime} ms";

        public ScanResult(PingReply reply, string mac)
        {
            Reply = reply;
            MAC = mac;
        }
    }


}
