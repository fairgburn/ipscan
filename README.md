# ipscan

Quickly scans a range of IP addresses.  
Garbage collector is forced to run after the scan, but still be mindful of using huge ranges. This can hog up a lot of memory until the scan is finished.  
This targets .NET Core 2.1, for a standalone Windows .exe you can either build for .NET Framework or right click on the project in Visual Studio and click Publish.

Example of scanning a big range:

![demo](./img/demo.gif)